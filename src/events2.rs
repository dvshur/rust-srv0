// // a client that has not been subscribed to any topics for some time
// // is considered to be idle and is disconnected

// /// --== TYPES ==--
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct Topic {
//     #[prost(oneof="topic::Topic", tags="1, 2")]
//     pub topic: ::std::option::Option<topic::Topic>,
// }
// pub mod topic {
//     #[derive(Clone, PartialEq, ::prost::Message)]
//     pub struct MatcherOrderbookParams {
//         #[prost(bytes, tag="1")]
//         pub amount_asset: std::vec::Vec<u8>,
//         #[prost(bytes, tag="2")]
//         pub price_asset: std::vec::Vec<u8>,
//     }
//     #[derive(Clone, PartialEq, ::prost::Message)]
//     pub struct MatcherBalancesParams {
//         #[prost(bytes, tag="1")]
//         pub address: std::vec::Vec<u8>,
//     }
//     #[derive(Clone, PartialEq, ::prost::Oneof)]
//     pub enum Topic {
//         #[prost(message, tag="1")]
//         MatcherOrderbook(MatcherOrderbookParams),
//         #[prost(message, tag="2")]
//         MatcherBalances(MatcherBalancesParams),
//     }
// }
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct Subscription {
//     #[prost(string, tag="1")]
//     pub id: std::string::String,
//     #[prost(message, optional, tag="2")]
//     pub topic: ::std::option::Option<Topic>,
//     #[prost(uint64, tag="3")]
//     pub subscription_timestamp: u64,
//     #[prost(uint64, tag="4")]
//     pub expiration_timestamp: u64,
// }
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct Error {
//     #[prost(uint32, tag="1")]
//     pub code: u32,
//     #[prost(string, tag="2")]
//     pub message: std::string::String,
//     /// error origin, if known
//     #[prost(message, optional, tag="3")]
//     pub topic: ::std::option::Option<Topic>,
//     #[prost(string, tag="4")]
//     pub subscription_id: std::string::String,
//     #[prost(map="string, bytes", tag="255")]
//     pub attributes: ::std::collections::HashMap<std::string::String, std::vec::Vec<u8>>,
// }
// /// --== EVENTS ==--
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct Unsubscribed {
//     #[prost(message, optional, tag="1")]
//     pub from: ::std::option::Option<Subscription>,
//     #[prost(enumeration="unsubscribed::Reason", tag="2")]
//     pub reason: i32,
//     /// empty if ok
//     #[prost(message, repeated, tag="255")]
//     pub errors: ::std::vec::Vec<Error>,
// }
// pub mod unsubscribed {
//     #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
//     #[repr(i32)]
//     pub enum Reason {
//         Unknown = 0,
//         Error = 1,
//         UserRequest = 2,
//         SubscriptionExpired = 3,
//         AccessTokenExpired = 4,
//     }
// }
// /// ¯\_(ツ)_/¯
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct Oops {
//     #[prost(message, repeated, tag="1")]
//     pub errors: ::std::vec::Vec<Error>,
// }
// // --== RPC ==--

// /// batch subscribe
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct SubscribeRequest {
//     #[prost(message, repeated, tag="1")]
//     pub to: ::std::vec::Vec<subscribe_request::Request>,
//     /// can be empty for public Topics
//     #[prost(string, tag="2")]
//     pub access_token: std::string::String,
// }
// pub mod subscribe_request {
//     #[derive(Clone, PartialEq, ::prost::Message)]
//     pub struct Request {
//         #[prost(message, optional, tag="1")]
//         pub topic: ::std::option::Option<super::Topic>,
//         /// can be no longer than some upper bound
//         #[prost(uint64, tag="2")]
//         pub expiration_timestamp: u64,
//     }
// }
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct SubscribeResponse {
//     #[prost(message, repeated, tag="1")]
//     pub results: ::std::vec::Vec<subscribe_response::Result>,
// }
// pub mod subscribe_response {
//     #[derive(Clone, PartialEq, ::prost::Message)]
//     pub struct Result {
//         #[prost(message, optional, tag="1")]
//         pub subscription: ::std::option::Option<super::Subscription>,
//         /// empty if ok
//         #[prost(message, repeated, tag="255")]
//         pub errors: ::std::vec::Vec<super::Error>,
//     }
// }
// /// batch unsubscribe
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct UnsubscribeRequest {
//     #[prost(string, repeated, tag="1")]
//     pub from_ids: ::std::vec::Vec<std::string::String>,
// }
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct UnsubscribeResponse {
//     #[prost(message, repeated, tag="1")]
//     pub results: ::std::vec::Vec<unsubscribe_response::Result>,
// }
// pub mod unsubscribe_response {
//     #[derive(Clone, PartialEq, ::prost::Message)]
//     pub struct Result {
//         #[prost(message, optional, tag="1")]
//         pub ended_subscription: ::std::option::Option<super::Subscription>,
//         /// empty if ok
//         #[prost(message, repeated, tag="255")]
//         pub errors: ::std::vec::Vec<super::Error>,
//     }
// }
// /// list user subscriptions with filters
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct GetActiveSubscriptionsRequest {
//     /// filters
//     ///
//     /// probably SQL IN semantic
//     #[prost(string, repeated, tag="1")]
//     pub ids: ::std::vec::Vec<std::string::String>,
//     /// get all subscriptions that expire soon in order to renew them
//     #[prost(uint64, tag="2")]
//     pub expire_before: u64,
// }
// #[derive(Clone, PartialEq, ::prost::Message)]
// pub struct GetActiveSubscriptionsResponse {
//     /// sorted by either subscription_timestamp or expiration_timestamp
//     #[prost(message, repeated, tag="1")]
//     pub subscriptions: ::std::vec::Vec<Subscription>,
//     /// empty if ok
//     #[prost(message, repeated, tag="255")]
//     pub errors: ::std::vec::Vec<Error>,
// }
