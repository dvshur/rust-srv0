use crate::errors::SubscriptionError;

use async_trait::async_trait;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver};
use tokio::time;

#[derive(Debug)]
struct Update<T> {
    id: usize,
    update: T,
}

use Update as UpdateTmpl;

#[async_trait]
trait Provider<Topic, Snapshot, Update> {
    async fn subscribe(
        &self,
        topic: Topic,
    ) -> Result<(Snapshot, UnboundedReceiver<UpdateTmpl<Update>>), SubscriptionError>;
}

#[derive(Debug)]
struct DummySnapshot(String);

#[derive(Debug)]
struct DummyUpdate(String);

// todo current id via AtomicUsize
struct DummyProvider {}

#[async_trait]
impl Provider<(), DummySnapshot, DummyUpdate> for DummyProvider {
    async fn subscribe(
        &self,
        _topic: (),
    ) -> Result<(DummySnapshot, UnboundedReceiver<Update<DummyUpdate>>), SubscriptionError> {
        let (tx, rx) = unbounded_channel();

        tokio::spawn(async move {
            let mut i = 0;
            while let Ok(_) = tx.send(UpdateTmpl {
                id: i,
                update: DummyUpdate(String::from("UPDATE")),
            }) {
                time::delay_for(time::Duration::from_secs(1)).await;
            }
            i += 1;
        });

        Ok((DummySnapshot(String::from("SNAPSHOT")), rx))
    }
}
