#![feature(try_blocks)]

mod providers;
mod errors;

use futures::select_biased;
use futures::{FutureExt, StreamExt};
use tokio::sync::mpsc;
use tokio::time;
use warp::ws;
use warp::ws::WebSocket;
use warp::Filter;

#[tokio::main]
async fn main() {
    let ws = warp::path("ws")
        .and(warp::ws())
        .map(|ws: warp::ws::Ws| ws.on_upgrade(move |socket| user_connected(socket)));

    let html = warp::path::end().and(warp::fs::file("src/index.html"));

    let routes = html.or(ws);

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await
}

// #[derive(Debug)]
// struct MatcherOrderbookMessage {
//     id: u8,
//     amount_asset: String,
//     prica_asset: String,
// }

#[derive(Debug)]
struct Ping;
#[derive(Debug)]
struct Pong;
#[derive(Debug)]
struct Close;

// todo maybe separate into incoming and outgoing messages
#[derive(Debug)]
enum Message {
    Text(String),
    Binary(Vec<u8>),
    Ping(Ping),
    Pong(Pong),
    Close(Close),
}

async fn start_session(
    source: mpsc::UnboundedReceiver<Message>,
    sink: mpsc::UnboundedSender<Message>,
) {
    let (pongs_tx, pongs_rx) = mpsc::unbounded_channel();
    let (unknown_tx, unknown_rx) = mpsc::unbounded_channel();

    let mut source = source;
    tokio::spawn(async move {
        while let Some(msg) = source.next().await {
            match msg {
                Message::Pong(p) => {
                    if let Err(e) = pongs_tx.send(p) {
                        eprintln!("Error while sending to pongs: {}", e);
                    }
                }
                Message::Text(t) => {
                    if let Err(e) = unknown_tx.send(t) {
                        eprintln!("Error while sending to unknown channels: {}", e);
                    }
                }
                _ => (),
            }
        }
    });

    tokio::spawn(async move {
        let mut unknown_rx = unknown_rx;
        while let Some(msg) = unknown_rx.next().await {
            println!("unknown msg: {}", msg);
        }
    });

    // this will keep connection alive as long as ping/pongs are exchanged
    handle_pong(pongs_rx, sink).await;
}

// todo reset timeout on premature client pings
async fn handle_pong(pongs: mpsc::UnboundedReceiver<Pong>, sink: mpsc::UnboundedSender<Message>) {
    let mut pongs = pongs;
    loop {
        time::delay_for(time::Duration::from_secs(5)).await;

        if let Err(e) = sink.send(Message::Ping(Ping)) {
            eprintln!("Error sending ping: {}", e);
            break;
        }

        println!("ping");

        select_biased! {
            _ = pongs.next().fuse() => println!("pong"),
            _ = time::delay_for(time::Duration::from_secs(5)).fuse() => break,
        }
    }
}

// --- === NETWORK === ---
async fn user_connected(ws: WebSocket) {
    println!("Connected");

    // Split the socket into a sender and receive of messages.
    let (ws_tx, mut ws_rx) = ws.split();

    // send to socket
    // todo send all necessary message types
    let (tx, rx_proxy) = mpsc::unbounded_channel();
    tokio::spawn(
        rx_proxy
            .filter_map(|msg: Message| async {
                match msg {
                    Message::Ping(_) => Some(ws::Message::ping(Vec::new())),
                    Message::Text(t) => Some(ws::Message::text(t)),
                    _ => None,
                }
            })
            .map(|x| Ok(x))
            .forward(ws_tx)
            .map(|res| {
                if let Err(e) = res {
                    eprintln!("Error while sinding to socket: {}", e);
                }
            }),
    );

    // receive from socket
    let (tx_proxy, rx) = mpsc::unbounded_channel();
    tokio::spawn(async move {
        // todo will this destructure fail silently and leave connection hanging?
        while let Some(Ok(msg)) = ws_rx.next().await {
            let message = match msg.to_str() {
                Ok(s) => Message::Text(String::from(s)),
                _ if msg.is_ping() => Message::Ping(Ping),
                _ if msg.is_pong() => Message::Pong(Pong),
                _ if msg.is_close() => Message::Close(Close),
                _ => Message::Binary(msg.into_bytes()),
            };

            if let Err(e) = tx_proxy.send(message) {
                println!("Failed to send message from socket to tx_proxy: {}", e);
                break;
            }
        }
    });

    // this await will last for as long as the connection needs to be held
    start_session(rx, tx).await;

    println!("User /probably_need_some_id/ disconnected");
}
